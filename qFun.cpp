#include <iostream>
#include <cmath>
#include <iomanip>
#include "qFun.h"
using namespace std;

void qFunction::read()
{
    cout << "Wprowadz wartosci poszczegolnych wspolczynnikow :" << endl;
    cout << "a (a*x^2) = ";
    cin  >> a;
	if(a==0)
    {
        cout << "To nie jest funkcja kwadratowa !" << endl;
		a=1; b=2; c=1;
        return;
    }
    cout << endl << "b (b*x) = ";
    cin  >> b;
    cout << endl << "c (wyraz wolny) = ";
    cin  >> c;
}
void qFunction::solve()
{
    double delta = b*b-4*a*c;
	x1 = (-b+sqrt(delta))/(2*a);
    x2 = (-b-sqrt(delta))/(2*a);
	cout.precision(4);
    if(delta < 0)
        cout << "Ta funkcja nie ma miejsc zerowych !" << endl;
    else if(delta == 0)
    {
		if (b == 0)
		x1=0;
        cout << "Funkcja ma 1 miejsce zerowe : \nx0 = " << x1 << endl;
    }

    else
    {
        cout << "Funkcja ma 2 miejsca zerowe : \nx1 = " << x1;
        cout << endl << "x2 = "<< x2 << endl;
    }
    
}

qFunction::qFunction(double x, double y, double z)
{
    a = x;
    b = y;
    c = z;
}

void qFunction::show()
{
	cout << "Wczytana funkcja : ";
	if(a==-1) 
		cout <<"-";
	else if (a!=1) 
		cout << a; 
	cout << "x^2";    
	if (b>0)
        cout << "+";
	else if (b==-1)
		cout << "-";
   	if(b!=1 && b!=0 && b!=-1)
        cout << b;
	if(b!=0)
		cout << "x ";
    if (c>0)
        cout << "+";
    if(c!=0)
        cout << c;
    cout << endl;
}
