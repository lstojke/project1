project: main.o qFun.o
	g++ -g main.o qFun.o -o project

main.o: main.cpp qFun.cpp
	g++ -c -g main.cpp

qFun.o: qFun.cpp qFun.h
	g++ -c -g qFun.cpp
