#include <iostream>
#include "qFun.h"
#include <cmath>
using namespace std;

int main()
{
	qFunction fun;
	int choice = 1;
	
	do{
		system("clear");
		cout << "Obliczanie miejsc zerowych funkcji kwadratowej" << endl << endl;
		fun.show();
		cout << "----------------------------------------------" << endl;
		cout << "1 - Podaj wspolczynniki funkcji" << endl << "2 - wyswietl miejsca zerowe funkcji";
		cout << endl << "0 - Wyjscie" << endl;
		cout << "----------------------------------------------" << endl;
		cin >> choice;
	    if(choice == 1)
		{
	    	fun.read();
			cout << " Wcisnij dowolny klawisz, aby kontynuowac";
			getchar();getchar();
		}
		if(choice == 2)
		{
			fun.solve();
			cout << " Wcisnij dowolny klawisz, aby kontynuowac";
			getchar();getchar();
		}	
	}while(choice != 0);
    return 0;
}
